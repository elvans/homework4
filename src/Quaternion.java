import java.text.DecimalFormat;

/** Quaternions. Basic operations. */
public class Quaternion {

    private double a;
    private double b;
    private double c;
    private double d;

   /** Constructor from four double values.
    * @param a real part
    * @param b imaginary part i
    * @param c imaginary part j
    * @param d imaginary part k
    */
   public Quaternion (double a, double b, double c, double d) {
      this.a = a;
      this.b = b;
      this.c = c;
      this.d = d;
   }

   /** Real part of the quaternion.
    * @return real part
    */
   public double getRpart() {
      return a;
   }

   /** Imaginary part i of the quaternion. 
    * @return imaginary part i
    */
   public double getIpart() {
      return b;
   }

   /** Imaginary part j of the quaternion. 
    * @return imaginary part j
    */
   public double getJpart() {
      return c;
   }

   /** Imaginary part k of the quaternion. 
    * @return imaginary part k
    */
   public double getKpart() {
      return d;
   }

   /** Conversion of the quaternion to the string.
    * @return a string form of this quaternion: 
    * "a+bi+cj+dk"
    * (without any brackets)
    */
   @Override
   public String toString() {
       return a + "+" + b + "i+" + c + "j+" + d + "k";
   }

   /** Conversion from the string to the quaternion. 
    * Reverse to <code>toString</code> method.
    * @throws IllegalArgumentException if string s does not represent 
    *     a quaternion (defined by the <code>toString</code> method)
    * @param s string of form produced by the <code>toString</code> method
    * @return a quaternion represented by string s
    */
   public static Quaternion valueOf (String s) {
       s = s.replace("-", "+-").replace("++", "+"); // replace all -, with +-, and avoid double +
       if(s.substring(0, 1).equals("+")){ s = s.substring(1);}; // if first element after replacement now has +-, then string begins from 2nd element
       String[] listOfValues = s.split("\\+");
       double[] numbers = {0, 0, 0, 0};
       for (String listOfValue : listOfValues) {
          StringBuilder elementStr = new StringBuilder(listOfValue);
          switch (elementStr.substring(elementStr.length() - 1)) {  // check which quanternion part is this
             case "i":
                 if(numbers[1] != 0.){throw new IllegalArgumentException("Not a guanternion!"); }
                 numbers[1] = Double.parseDouble(elementStr.deleteCharAt(elementStr.length() - 1).toString());
                 break;
             case "j":
                 if(numbers[2] != 0.){throw new IllegalArgumentException("Not a guanternion!"); }
                 numbers[2] = Double.parseDouble(elementStr.deleteCharAt(elementStr.length() - 1).toString());
                 break;
             case "k":
                 if(numbers[3] != 0.){throw new IllegalArgumentException("Not a guanternion!"); }
                 numbers[3] = Double.parseDouble(elementStr.deleteCharAt(elementStr.length() - 1).toString());
                 break;
             default:
                 try{ // if part is not i, j, k or not parsable - throw exception
                     if(numbers[0] != 0.){throw new IllegalArgumentException("Not a guanternion!"); }
                     numbers[0] = Double.parseDouble(elementStr.toString());
                     break;
                 }catch (NumberFormatException e){
                     throw new IllegalArgumentException("Invalid quanternion!");
                 }
          }
       }
       return new Quaternion(numbers[0], numbers[1], numbers[2], numbers[3]);
   }


   /** Clone of the quaternion.
    * @return independent clone of <code>this</code>
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
       return new Quaternion(a, b, c, d);
   }

   /** Test whether the quaternion is zero. 
    * @return true, if the real part and all the imaginary parts are (close to) zero
    */
   public boolean isZero() {
      return equals(new Quaternion(0., 0., 0., 0.));
   }

   /** Conjugate of the quaternion. Expressed by the formula 
    *     conjugate(a+bi+cj+dk) = a-bi-cj-dk
    * @return conjugate of <code>this</code>
    */
   public Quaternion conjugate() {
      return new Quaternion(a, -b, -c, -d);
   }

   /** Opposite of the quaternion. Expressed by the formula 
    *    opposite(a+bi+cj+dk) = -a-bi-cj-dk
    * @return quaternion <code>-this</code>
    */
   public Quaternion opposite() {
       return new Quaternion(-a, -b, -c, -d);
   }

   /** Sum of quaternions. Expressed by the formula 
    *    (a1+b1i+c1j+d1k) + (a2+b2i+c2j+d2k) = (a1+a2) + (b1+b2)i + (c1+c2)j + (d1+d2)k
    * @param q addend
    * @return quaternion <code>this+q</code>
    */
   public Quaternion plus (Quaternion q) {
      return new Quaternion(q.a + this.a, q.b + this.b, q.c + this.c, q.d + this.d);
   }

   /** Product of quaternions. Expressed by the formula
    *  (a1+b1i+c1j+d1k) * (a2+b2i+c2j+d2k) = (a1a2-b1b2-c1c2-d1d2) + (a1b2+b1a2+c1d2-d1c2)i +
    *  (a1c2-b1d2+c1a2+d1b2)j + (a1d2+b1c2-c1b2+d1a2)k
    * @param q factor
    * @return quaternion <code>this*q</code>
    */
   public Quaternion times (Quaternion q) {
      return new Quaternion((a * q.a - b * q.b - c * q.c - d * q.d), (a * q.b + b * q.a + c * q.d - d * q.c),
              (a * q.c - b * q.d + c * q.a + d * q.b), (a * q.d + b * q.c - c * q.b + d * q.a));
   }

   /** Multiplication by a coefficient.
    * @param r coefficient
    * @return quaternion <code>this*r</code>
    */
   public Quaternion times (double r) {
      return new Quaternion(r * a, r * b, r * c, r * d);
   }

   /** Inverse of the quaternion. Expressed by the formula
    *     1/(a+bi+cj+dk) = a/(a*a+b*b+c*c+d*d) + 
    *     ((-b)/(a*a+b*b+c*c+d*d))i + ((-c)/(a*a+b*b+c*c+d*d))j + ((-d)/(a*a+b*b+c*c+d*d))k
    * @return quaternion <code>1/this</code>
    */
   public Quaternion inverse() {
       if(a == 0 && b == 0 && c == 0 && d == 0){ throw new RuntimeException("Invalid Quaternion!"); }
       DecimalFormat df = new DecimalFormat("#.####");
       double newA = Double.parseDouble(df.format(a/(a*a+b*b+c*c+d*d)));
       double newB = Double.parseDouble(df.format(((-b)/(a*a+b*b+c*c+d*d))));
       double newC = Double.parseDouble(df.format((-c)/(a*a+b*b+c*c+d*d)));
       double newD = Double.parseDouble(df.format((-d)/(a*a+b*b+c*c+d*d)));
       if(df.format(newA).equals("-0.0")) { newA = 0.; }
       if(df.format(newB).equals("-0.0")) { newA = 0.; }
       if(df.format(newC).equals("-0.0")) { newA = 0.; }
       if(df.format(newD).equals("-0.0")) { newA = 0.; }
       return new Quaternion(newA, newB, newC, newD);
   }

   /** Difference of quaternions. Expressed as addition to the opposite.
    * * @param q subtrahend
    *  * @return quaternion <code>this-q</code>
    */
   public Quaternion minus (Quaternion q) {
       double rPart = this.getRpart() - q.getRpart();
       double iPart = this.getIpart() - q.getIpart();
       double jPart = this.getJpart() - q.getJpart();
       double kPart = this.getKpart() - q.getKpart();
       return new Quaternion(rPart, iPart, jPart, kPart);
   }

   /** Right quotient of quaternions. Expressed as multiplication to the inverse.
    * @param q (right) divisor
    * @return quaternion <code>this*inverse(q)</code>
    */
   public Quaternion divideByRight (Quaternion q) {
      DecimalFormat df = new DecimalFormat("#.###");
      Quaternion quat = times(q.inverse());
      return new Quaternion(Double.parseDouble(df.format(quat.a)), Double.parseDouble(df.format(quat.b)),
              Double.parseDouble(df.format(quat.c)), Double.parseDouble(df.format(quat.d)));
   }

   /** Left quotient of quaternions.
    * @param q (left) divisor
    * @return quaternion <code>inverse(q)*this</code>
    */
   public Quaternion divideByLeft (Quaternion q) {
      DecimalFormat df = new DecimalFormat("#.###");
      Quaternion quat = q.inverse().times(this);
      return new Quaternion(Double.parseDouble(df.format(quat.a)), Double.parseDouble(df.format(quat.b)),
              Double.parseDouble(df.format(quat.c)), Double.parseDouble(df.format(quat.d)));
   }
   
   /** Equality test of quaternions. Difference of equal numbers
    *     is (close to) zero.
    * @param qo second quaternion
    * @return logical value of the expression <code>this.equals(qo)</code>
    */
   @Override
   public boolean equals (Object qo) {
      double closeToZero = 0.00001;
      Quaternion quat = (Quaternion)qo; // object qo to quanternion
      return Math.abs(a - quat.getRpart()) < closeToZero && Math.abs(b - quat.getIpart()) < closeToZero //check if abs is close to 0
              && Math.abs(c - quat.getJpart()) < closeToZero && Math.abs(d - quat.getKpart()) < closeToZero;
   }

   /** Dot product of quaternions. (p*conjugate(q) + q*conjugate(p))/2
    * @param q factor
    * @return dot product of this and q
    */
   public Quaternion dotMult (Quaternion q) {
      Quaternion m = times(q.conjugate()).plus(q.times(conjugate()));
      return new Quaternion(m.a / 2., m.b / 2., m.c / 2., m.d / 2.);
   }

   /** Integer hashCode has to be the same for equal objects.
    * @return hashcode
    */
   @Override
   public int hashCode() {
      return toString().hashCode();
   }

   /** Norm of the quaternion. Expressed by the formula 
    *     norm(a+bi+cj+dk) = Math.sqrt(a*a+b*b+c*c+d*d)
    * @return norm of <code>this</code> (norm is a real number)
    */
   public double norm() {
      return Math.sqrt(a*a+b*b+c*c+d*d);
   }

   /** Main method for testing purposes. 
    * @param arg command line parameters
    */
   public static void main (String[] arg) {
      Quaternion arv1 = new Quaternion (-1., 1, 2., -2.);
      if (arg.length > 0)
         arv1 = valueOf (arg[0]);
      System.out.println ("first: " + arv1.toString());
      System.out.println ("real: " + arv1.getRpart());
      System.out.println ("imagi: " + arv1.getIpart());
      System.out.println ("imagj: " + arv1.getJpart());
      System.out.println ("imagk: " + arv1.getKpart());
      System.out.println ("isZero: " + arv1.isZero());
      System.out.println ("conjugate: " + arv1.conjugate());
      System.out.println ("opposite: " + arv1.opposite());
      System.out.println ("hashCode: " + arv1.hashCode());
      Quaternion res = null;
      try {
         res = (Quaternion)arv1.clone();
      } catch (CloneNotSupportedException e) {};
      System.out.println ("clone equals to original: " + res.equals (arv1));
      System.out.println ("clone is not the same object: " + (res!=arv1));
      System.out.println ("hashCode: " + res.hashCode());
      res = valueOf (arv1.toString());
      System.out.println ("string conversion equals to original: " 
         + res.equals (arv1));
      Quaternion arv2 = new Quaternion (1., -2.,  -1., 2.);
      if (arg.length > 1)
         arv2 = valueOf (arg[1]);
      System.out.println ("second: " + arv2.toString());
      System.out.println ("hashCode: " + arv2.hashCode());
      System.out.println ("equals: " + arv1.equals (arv2));
      res = arv1.plus (arv2);
      System.out.println ("plus: " + res);
      System.out.println ("times: " + arv1.times (arv2));
      System.out.println ("minus: " + arv1.minus (arv2));
      double mm = arv1.norm();
      System.out.println ("norm: " + mm);
      System.out.println ("inverse: " + arv1.inverse());
      System.out.println ("divideByRight: " + arv1.divideByRight (arv2));
      System.out.println ("divideByLeft: " + arv1.divideByLeft (arv2));
      System.out.println ("dotMult: " + arv1.dotMult (arv2));
   }
}
// end of file
